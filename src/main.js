var colors = {
  light: [13, 13, 13],
  dark: [0, 0, 0]
}

var boardSize
var initSize = 0.11
var dimension = 7
var boardArray = create2DArray(dimension, dimension, 0, false)
var direction = 0
var clusters = calculateClusters(getCells(boardArray))
var font
var state = false

var player = {
  x: 0,
  y: 0
}

function preload() {
  font = loadFont('ttf/RobotoMono-Medium.ttf')
}

function setup() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function draw() {
  createCanvas(windowWidth, windowHeight)
  background(colors.light)
  rectMode(CENTER)
  colorMode(RGB, 255, 255, 255, 1)
  textAlign(CENTER, CENTER)
  textFont(font)

  fill(colors.dark)
  noStroke()
  rect(windowWidth * 0.5, windowHeight * 0.5, boardSize, boardSize)

  for (var i = 0; i < clusters.length; i++) {
    for (var j = 0; j < clusters[i].length; j++) {
      push()
      translate(windowWidth * 0.5 + (clusters[i][j][0] - Math.floor(dimension * 0.5)) * boardSize * initSize, windowHeight * 0.5 + (clusters[i][j][1] - Math.floor(dimension * 0.5)) * boardSize * initSize)
      if (state === false) {
        fill(128 -  128 * ((j + 1) / clusters[i].length))
        stroke(255)
        strokeWeight(boardSize * initSize * 0.05)
        rect(0, 0, boardSize * initSize, boardSize * initSize)
      }
      fill(255)
      noStroke()
      textSize(boardSize * initSize * 0.5)
      text(i, 0, 0 * boardSize * initSize * 0.0625)
      textSize(boardSize * initSize * 0.2)
      text(j, boardSize * initSize * 0.25, -boardSize * initSize * 0.25)
      pop()
    }
  }

  if (Math.floor(7 * abs(tan(frameCount * Math.random()))) % 7 === 0) {
    direction = Math.floor(Math.random() * 4)
  }
  state = [true, false][Math.floor(Math.random() * 2)]
  if (direction === 0) {
    if (player.y < 1) {
      player.y = dimension - 1
    } else {
      player.y--
    }
  }
  if (direction === 1) {
    if (player.y > dimension - 2) {
      player.y = 0
    } else {
      player.y++
    }
  }
  if (direction === 2) {
    if (player.x < 1) {
      player.x = dimension - 1
    } else {
      player.x--
    }
  }
  if (direction === 3) {
    if (player.x > dimension - 2) {
      player.x = 0
    } else {
      player.x++
    }
  }
  boardArray[player.x][player.y] = (boardArray[player.x][player.y] + 1) % 2
  clusters = calculateClusters(getCells(boardArray))
}

function windowResized() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function calculateVertices(cluster) {
  var vertices = []

  return vertices
}

function calculateClusters(cells) {
  var clusters = []
  for (var i = 0; i < cells.length; i++) {
    var initCell = cells[i]
    var cluster = []
    cluster.push(initCell)
    for (var k = 0; k < cells.length; k++) {
      var neighbours = getNeighbours(cluster)
      for (var j = 0; j < neighbours.length; j++) {
        cluster.push(neighbours[j])
      }
    }
    clusters.push(cluster)
    cluster = []
  }
  for (var i = 0; i < clusters.length; i++) {
    clusters[i].sort(function(a, b) {
      return a[0] < b[0]? -1 : (a[0] == b[0]? 0 : 1)
    })
    clusters[i].sort(function(a, b) {
      return a[1] < b[1]? -1 : (a[1] == b[1]? 0 : 1)
    })
  }
  clusters = multiDimensionalUnique(clusters)
  return clusters
}

function getCells(array) {
  var clusters = []
  var cells = []
  for (var i = 0; i < array.length; i++) {
    for (var j = 0; j < array[i].length; j++) {
      if (array[i][j] === 1) {
        cells.push([i, j])
      }
    }
  }
  return cells
}

function getNeighbours(flood) {
  var neighbours = []
  for (var i = 0; i < flood.length; i++) {
    var posX = flood[i][0]
    var posY = flood[i][1]
    if (posX > 0) {
      if (isArrayInArray(flood, [posX - 1, posY]) === false && boardArray[posX - 1][posY] !== 0) {
      neighbours.push([posX - 1, posY])
      }
    }
    if (posX < dimension - 1) {
      if (isArrayInArray(flood, [posX + 1, posY]) === false && boardArray[posX + 1][posY] !== 0) {
        neighbours.push([posX + 1, posY])
      }
    }
    if (posY > 0) {
      if (isArrayInArray(flood, [posX, posY - 1]) === false && boardArray[posX][posY - 1] !== 0) {
        neighbours.push([posX, posY - 1])
      }
    }
    if (posY < dimension - 1) {
      if (isArrayInArray(flood, [posX, posY + 1]) === false && boardArray[posX][posY + 1] !== 0) {
        neighbours.push([posX, posY + 1])
      }
    }
  }
  neighbours = multiDimensionalUnique(neighbours)
  return neighbours
}

function multiDimensionalUnique(arr) {
  var uniques = []
  var itemsFound = {}
  for (var i = 0, l = arr.length; i < l; i++) {
    var stringified = JSON.stringify(arr[i])
    if (itemsFound[stringified]) {
      continue
    }
    uniques.push(arr[i])
    itemsFound[stringified] = true
  }
  return uniques
}

function isArrayInArray(arr, item) {
  var item_as_string = JSON.stringify(item)
  var contains = arr.some(function(ele) {
    return JSON.stringify(ele) === item_as_string
  })
  return contains
}

function create2DArray(numRows, numCols, init, bool) {
  var array = [];
  for (var i = 0; i < numRows; i++) {
    var columns = []
    for (var j = 0; j < numCols; j++) {
      if (bool === true) {
        columns[j] = init
      } else {
        columns[j] = Math.floor(Math.random() * 2)
      }
    }
    array[i] = columns
  }
  return array
}
